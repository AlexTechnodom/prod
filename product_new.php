<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <title>PRODUCT ADD</title>
    <link rel="stylesheet" href="includes/new.css">
    <script>
        function SelectType(value) {
            switch (value) {
                case "Size":
                    document.getElementById('id-size').style.display = 'block';
                    document.getElementById('id-hwl').style.display = 'none';
                    document.getElementById('id-weight').style.display = 'none';
                    break;
                case "HWL":
                    document.getElementById('id-hwl').style.display = 'block';
                    document.getElementById('id-size').style.display = 'none';
                    document.getElementById('id-weight').style.display = 'none';
                    break;
                case "Weight":
                    document.getElementById('id-weight').style.display = 'block';
                    document.getElementById('id-size').style.display = 'none';
                    document.getElementById('id-hwl').style.display = 'none';
                    break;
            }
        }
    </script>
</head>

<body>
    <?php $page_title='PRODUCT NEW'; 
    echo '<form action="product_add.php" method="post" accept-charset="utf-8">
           <legend>Product Add
              <input type="submit" value="Save"></p>
           </legend>
           <p>SKU   <input name="sku" type="text"><br>
           <p>Name  <input name="name" type="text"><br>
           <p>Price <input name="price" type="text"><br>
           <p><label>Type Switcher</label>
              <select name="types" OnChange="SelectType(value)">
                 <option selected disabled value="D">Select type</option>
                 <option value="Size">Type Size</option>
                 <option value="HWL">Type H \ W \ L</option>
                 <option value="Weight">Type Weight</option>
              </select>
           <div id="id-size" class="cl-size">
              <label for="size">Size</label>
              <input type="text" name="size" id="size" placeholder="Enter the size"> 
           </div>
           <div id="id-hwl" class="cl-hwl">
              <label for="height">Height</label>
              <input type="text" name="height" id="height" placeholder="Enter the height"><br>
              <label for="width">Width</label>
              <input type="text" name="width" id="width" placeholder="Enter the width"><br>
              <label for="length">Length</label>
              <input type="text" name="length" id="length" placeholder="Enter the length"><br>
              <small id="hwlHelp" class="cl-hwlHelp">Please provide dimensions in HxWxL format</small> 
           </div>
           <div id="id-weight" class="cl-weight">
              <label for="weight">Weight</label>
              <input type="text" name="weight" id="weight" placeholder="Enter the weight"> 
           </div>
          </form>'; ?>
</body>

</html>