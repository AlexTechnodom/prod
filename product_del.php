<?php
include_once("Database.php");

$a = new Database();
$con=$a->connect();

if ($con) {
	$resp=$a->delete($_GET['del_id']);

	if ($resp) {
		echo '<p>The entry has been deleted. Ok.</p>';
	} else {
		echo '<p>An error occurred during the deletion. Fail.</p>';
	}
} else {
	echo '<p>Database connection error!</p>';
}

