<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <title>PRODUCT LIST</title>
    <link rel="stylesheet" href="includes/main.css">
    <script>
        function funDelete() {
            var selD = document.getElementById("selDelete");
            var valD = selD.options[selD.selectedIndex].value;
            switch (valD) {
                case "ALL":
                    var y = new XMLHttpRequest();
                    y.open("GET", "product_del.php?del_id=0", true);
                    y.send();
                    break;
                case "DEL":
                    var arrInp = document.getElementsByTagName('input');
                    for (var i = arrInp.length - 1; i >= 0; i--) {
                        if ((arrInp[i].getAttribute('type') == 'checkbox') && (arrInp[i].checked)) {
                            var x = arrInp[i].getAttribute('value');
                            var y = new XMLHttpRequest();
                            y.open("GET", "product_del.php?del_id=" + x, true);
                            y.send();
                        }
                    }
                    break;
            }
            location.reload(true);
        }
    </script>
</head>

<body>
    <legend>Product List
        <select id=selDelete name="remove">
            <option value="ALL">Mass Delete Action</option>
            <option value="DEL">Delete choise rows</option>
        </select>
        <input type="submit" value="Apply" onclick="funDelete()"> 
    </legend>

    <?php 
    include_once("Database.php");
    $table = 'test';

    $a = new Database();
    $con=$a->connect();

    if ($con) {
        $result=$a->select($table);
    } else {
        echo '<p>Database connection error!</p>';
    }

    if (mysqli_num_rows($result) > 0) {
    echo '
    <div id="list">';
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $last_field = 0;
        switch ($row['types']) {
            case "Size":
                $last_field = 'Size : ' . $row['size'] . " MB";
            break;
            case "HWL":
                $last_field = 'Dimension : ' . $row['height'] . "x" . $row['width'] . "x" . $row['length'];
            break;
            case "Weight":
                $last_field = 'Weight : ' . $row['weight'] . " KG";
            break;
        }
        echo '
        <div class="rows">' . '
            <input id="nomer" name="Nomer" type="checkbox" value=' . $row['id'] . '/>' . '
            <p>' . $row['sku'] . '</p>' . '
            <p>' . $row['name'] . '</p>' . '
            <p>' . $row['price'] . ' $' . '</p>' . '
            <p>' . $last_field . '</p>' . '</div>';
    }
    echo '</div>';
    } else {
        echo '
        <p>There are no records in table!</p>';
    }
    ?>

 </body>

</html>
