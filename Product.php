<?php
class Product
{
    public $sku;
    public $name;
    public $price;
    public $types;
    public $size;
    public $height;
    public $width;
    public $length;
    public $weight;

    public function setSku($Sku)
    {
        $this->sku = $Sku;
    }
    public function getSku()
    {
        return $this->sku;
    }

    public function setName($Name)
    {
        $this->name = $Name;
    }
    public function getName()
    {
        return $this->name;
    }

    public function setPrice($Price)
    {
        $this->price = $Price;
    }
    public function getPrice()
    {
        return $this->price;
    }

    public function setTypes($Types)
    {
        $this->types = $Types;
    }
    public function getTypes()
    {
        return $this->types;
    }

    public function setSize($Size)
    {
        $this->size = $Size;
    }
    public function getSize()
    {
        return $this->size;
    }

    public function setHeight($Height)
    {
        $this->height = $Height;
    }
    public function getHeight()
    {
        return $this->height;
    }

    public function setWidth($Width)
    {
        $this->width = $Width;
    }
    public function getWidth()
    {
        return $this->width;
    }

    public function setLength($Length)
    {
        $this->length = $Length;
    }
    public function getLength()
    {
        return $this->length;
    }

    public function setWeight($Weight)
    {
        $this->weight = $Weight;
    }
    public function getWeight()
    {
        return $this->weight;
    }
}