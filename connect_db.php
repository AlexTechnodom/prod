<?php
$host = 'localhost';
$user = 'root';
$password = '12345';
$database = 'test';
$charset = 'utf8mb4';

$pdoOptions = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false
);
 
try {
  $dbc = new PDO("mysql:host=$host;dbname=$database;charset=$charset", $user, $password, $pdoOptions);
} catch (PDOException $e) {
     throw new PDOException($e->getMessage(), (int)$e->getCode());
}
  
