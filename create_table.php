<?php
require('..\..\connect_db.php');
$sql = 'CREATE TABLE IF NOT EXISTS test ( ' . 'id INT UNSIGNED NOT NULL AUTO_INCREMENT ,' . 'sku VARCHAR(20) NOT NULL ,' . 'name VARCHAR(40) NOT NULL ,' . 'price VARCHAR(60) NOT NULL ,' . 'types VARCHAR(20) NOT NULL ,' . 'size int(10) DEFAULT NULL ,' . 'height int(20) DEFAULT NULL ,' . 'width int(20) DEFAULT NULL ,' . 'length int(20) DEFAULT NULL ,' . 'weight int(20) DEFAULT NULL ,' . 'PRIMARY KEY ( id ) ) ';
if (mysqli_query($dbc, $sql) === TRUE) {
	echo 'Таблица успешно создана';
} else {
	echo 'Ошибка создания таблицы: ' . mysqli_error($dbc);
}
mysqli_close($dbc);
