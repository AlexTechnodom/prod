<?php
function fail($str)
{
	echo "<p>The field is not filled - $str</p>";
	echo '<p><a href="product_new.php">To return to an input window</a>';
	exit();
}
if (!empty(trim($_POST['sku']))) {
	$sku = addslashes($_POST['sku']);
} else {
	fail('sku');
}
if (!empty(trim($_POST['name']))) {
	$name = addslashes($_POST['name']);
} else {
	fail('name');
}
if (!empty(trim($_POST['price']))) {
	$price = addslashes($_POST['price']);
} else {
	fail('price');
}
if (!empty(trim(isset($_POST['types'])))) {
	$types = addslashes($_POST['types']);
	switch ($types) {
		case 'Size':
			if (!empty(trim($_POST['size']))) {
				$size = addslashes($_POST['size']);
				$height = 0;
				$width = 0;
				$length = 0;
				$weight = 0;
			} else {
				fail('size');
			}
			break;

		case 'HWL':
			if (!empty(trim($_POST['height']))) {
				$height = addslashes($_POST['height']);
			} else {
				fail('height');
			}
			if (!empty(trim($_POST['width']))) {
				$width = addslashes($_POST['width']);
			} else {
				fail('width');
			}
			if (!empty(trim($_POST['length']))) {
				$length = addslashes($_POST['length']);
			} else {
				fail('length');
			}
			$size = 0;
			$weight = 0;
			break;

		case 'Weight':
			if (!empty(trim($_POST['weight']))) {
				$weight = addslashes($_POST['weight']);
				$size = 0;
				$height = 0;
				$width = 0;
				$length = 0;
			} else {
				fail('weight');
			}
			break;
	}
} else {
	fail('types');
}

include_once("Product.php");
$product = new Product();
$product->setSku($sku);
$product->setName($name);
$product->setPrice($price);
$product->setTypes($types);
$product->setSize($size);
$product->setHeight($height);
$product->setWidth($width);
$product->setLength($length);
$product->setWeight($weight);

include_once("Database.php");

$a = new Database();
$con=$a->connect();

if ($con) {
	$resp=$a->insert($product);

	if ($resp) {
		echo '<p>Record are added : ' . $sku . '. Ok.</p>';
	} else {
		echo '<p>Error add record : ' . $sku . '. Fail.</p>';
	}
} else {
	echo '<p>Database connection error!</p>';
}

echo '<p><a href="product_new.php">To add new entry</a></p>';
